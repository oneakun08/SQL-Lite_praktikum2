package smktelkom_mlg.sch.id.praktikum6_2_reza;

import android.os.Bundle;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

public class ShareActivity extends AppCompatActivity {
    ImageButton btnShare;
    EditText etText;
    String textToShare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);
        btnShare = findViewById(R.id.share);
        etText = findViewById(R.id.body);

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textToShare = etText.getText().toString();
                openShare(textToShare);
            }
        });

    }

    private void openShare(String textToShare) {
        String mimeType = "text/plain";
        String title = "Share";
        ShareCompat.IntentBuilder.from(this).setType(mimeType).setChooserTitle(title).setText(textToShare).startChooser();
    }
}
